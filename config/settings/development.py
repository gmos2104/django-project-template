"""
Settings for development environment
"""
from .base import *

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS += ["debug_toolbar"]

MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE

DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": lambda request: DEBUG}

MEDIA_URL = "/media/"

MEDIA_ROOT = BASE_DIR / "{{ project_name }}/media"
