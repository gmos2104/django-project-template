"""
Settings common to all environments
"""
import os

from pathlib import Path

from django.core.exceptions import ImproperlyConfigured

BASE_DIR = Path(__file__).resolve().parent.parent.parent

# Utilities


def get_env(key, cast_to=str, default=None):
    """
    Gets a given key from the environment.
    If no default value is provided and key does not exists raises an ImproperlyConfigured Error.
    Optional argument can be passed to parse the retrieved value from the environment.
    Available types:
        int
        bool
        str (default)
    """
    try:
        if default != None:
            value = os.environ.get(key, default)
        else:
            value = os.environ[key]

        if cast_to != str:
            return cast_to(value)

        return value
    except KeyError:
        error_msg = f"Set the {key} environment variable"
        raise ImproperlyConfigured(error_msg)


SECRET_KEY = get_env("SECRET_KEY")

# Application definition

INSTALLED_APPS = [
    "{{ project_name }}.core",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "{{ project_name }}/templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {"format": "{asctime} | {levelname} | {message}", "style": "{"},
        "detailed": {
            "format": "{asctime} | {levelname} | {name}:{funcName}:{lineno} | {message}",
            "style": "{",
        },
    },
    "handlers": {
        "app": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": BASE_DIR / "logs/app.log",
            "formatter": "detailed",
        },
        "django_info": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "filename": BASE_DIR / "logs/django.info.log",
            "formatter": "simple",
        },
        "django_error": {
            "level": "ERROR",
            "class": "logging.FileHandler",
            "filename": BASE_DIR / "logs/django.error.log",
            "formatter": "simple",
        },
        "console": {"class": "logging.StreamHandler", "formatter": "simple"},
    },
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": get_env("DJANGO_LOGGING_LEVEL", default="INFO"),
            "propagate": True,
        },
        "django.request": {
            "handlers": ["django_info", "django_error", "console"],
            "level": "INFO",
        },
        "django.server": {
            "handlers": ["django_info", "django_error", "console"],
            "level": "INFO",
        },
        "{{ project_name }}": {"handlers": ["app"], "level": "DEBUG"},
    },
}

WSGI_APPLICATION = "config.wsgi.application"

AUTH_USER_MODEL = "core.User"

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": get_env("DB_NAME"),
        "USER": get_env("DB_USER"),
        "PASSWORD": get_env("DB_PASSWORD"),
        "HOST": get_env("DB_HOST", default="localhost"),
        "PORT": get_env("DB_PORT", default="5432"),
        "TEST": {"NAME": get_env("DB_TEST_NAME", default="TEST_DB")},
    }
}

LANGUAGE_CODE = "en-US"

TIME_ZONE = "UTC"

USE_I18N = True

LOCALE_PATHS = [BASE_DIR / "{{ project_name }}/locale"]

USE_L10N = True

USE_TZ = True

STATIC_URL = "/static/"

STATICFILES_DIRS = [BASE_DIR / "{{ project_name }}/static"]
