import os

from django.core.management.base import CommandError
from django.core.management.commands import startapp


class Command(startapp.Command):
    """Changes 'startapp' base app directory to {{ project_name }} when not specified"""

    project_name = "{{ project_name }}"

    def handle(self, *args, **options):
        app_name = options.get("name")

        if options["directory"] is None:
            target_app = f"{self.project_name}/{app_name}"
            target_dir = os.path.join(os.getcwd(), target_app)

            try:
                os.makedirs(target_dir)
            except FileExistsError:
                raise CommandError(f"{target_dir} already exists")
            except OSError as error:
                raise CommandError(error)

            options["directory"] = target_app

        super().handle(*args, **options)
