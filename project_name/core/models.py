from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    """Base User model"""

    pass


class TimeStampedModel(models.Model):
    """Abstract class that adds creation/modification timestamp to the model"""

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
