FROM python:3.7-slim

ARG PUID=1000
ARG PGID=1000

ENV PUID ${PUID}
ENV PGID ${PGID}
ENV PYTHONUNBUFFERED 1

RUN apt-get update \
    && apt-get -y install gettext

RUN groupadd -g ${PGID} django \
    && useradd -u ${PUID} -g django -m django -s /bin/bash

ENV NVM_DIR /home/django/.nvm

USER django

WORKDIR /home/django/code

ADD requirements requirements

RUN pip install --user --no-warn-script-location -r requirements/development.txt

RUN mkdir -p ${NVM_DIR} \
    && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash \
    && . ${NVM_DIR}/nvm.sh \
    && nvm install --lts

CMD python manage.py runserver 0.0.0.0:8000

EXPOSE 8000
