# django-project-template

My project template for Django. Based on the recomendations of Two Scoops.

## How to use this template

`django-admin startproject --template https://gitlab.com/gmos2104/django-project-template/-/archive/master/django-project-template-master.zip YOUR_PROJECT_NAME`

Make sure you have the following environment vars available before starting the app using your prefered method:
- `SECRET_KEY`
- `DB_NAME`
- `DB_USER`
- `DB_PASSWORD`

Optional:
- `DB_HOST` (default is `localhost`)
- `DB_PORT` (default is `5432`)
- `DB_TEST_NAME` (default is `TEST_DB`)
- `DJANGO_LOGGING_LEVEL` to set the logging level for the `django` logger (default is `INFO`)
